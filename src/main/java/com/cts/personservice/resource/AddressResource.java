package com.cts.personservice.resource;


import com.cts.personservice.model.*;
import com.cts.personservice.service.AddressService;
import com.cts.personservice.service.PersonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/v1/persons/{personId}/addresses")
@Api("Set of endpoints for adding and Deleting Address.")
public class AddressResource {

    private static final Logger LOG = LoggerFactory.getLogger(AddressResource.class);

    private final AddressService addressService;

    public AddressResource( AddressService addressService) {
        this.addressService = addressService;
    }


    //3.Update Address
    @ApiOperation("add address. 404 if the person's identifier is not found.")
    @PutMapping(value = "/address")
    public ResponseEntity<Void> addAddress(@PathVariable String personId,
                                             @RequestBody Address address,
                                             @RequestHeader(value = "If-Match") String ifMatch,
                                             @RequestHeader(value = "X-Tenant-Id",required = false) String tenantId) {
        String version = addressService.addAddress(personId, ifMatch, address);
        LOG.info("Person update is successful, personId={}", personId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT)
                .eTag(version)
                .build();
    }

    @ApiOperation("Delete a address. 404 if the persons's identifier is not found.")
    @DeleteMapping(value = "/deleteAddress/{addressId}")
    public ResponseEntity<Void> deleteAddress(@PathVariable String personId,
                                              @PathVariable String addressId,
                                             @RequestHeader(value = "X-Tenant-Id",required = false) String tenantId) {
        addressService.deleteAddress(personId,addressId);
        LOG.info("address delete is successful, personId={}", personId);
        return ResponseEntity.status(HttpStatus.OK)
                .build();
    }

}
